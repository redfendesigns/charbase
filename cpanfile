requires 'JSON', '>= 2.00, < 2.80';
recommends 'JSON::XS', '2.0';
requires 'YAML::XS', '>= 0.69';
# requires 'Plack', '1.0'; # 1.0 or newer
# requires 'Catalyst'; # figure out what version of catalyst we want to pin to.
requires 'Template::Liquid';
