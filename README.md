# Charbase

This is a rewrite of our old Ruby webapp, Charbase. Due to code
quality, incompatabilities, and just general brokeness, we've taken
down that tool to be replaced this with this one. 

# Setting up your dev environment
## assumes you're in the repo root!
	cpan App::perlbrew
	perlbrew init
	perlbrew install perl-5.26.1
	perlbrew install-cpanm
	perlbrew use perl-5.26.1
	cpanm carton
	carton install
