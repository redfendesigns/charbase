#!/usr/bin/env sh

die () {
    printf '[!] fatal: %s\n' "$1-"
    exit 1
}
warning () {
	printf '[*] warn: %s\n' "$1-"
}
warningprompt () {
    warning "$1-"
    read 
}
source config

warning "test"
warningprompt "test ^c"
die "untested, run at your own risk!" # to run, comment out this line.

# prep environment
perlbrew install "$CHARBASE_PERL_VERSION" || die "failed to install local perl"
perlbrew install-cpanm
perlbrew use "$CHARBASE_PERL_VERSION"
cpanm Carton

# install local packages
carton install

# run our little test suite 
#carton exec ./runtests.pl || warningprompt "some tests failed, run at your own risk! (Press Control-C to exit, or enter to continue)"
#carton exec ./first-setup.pl
